//
//  ViewController.swift
//  Calculadora
//
//  Created by Victor Hugo Benitez Bosques on 02/12/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit

enum CalcOp : String{
    case add = "op-suma"
    case substract = "op-resta"
    case multiply = "op-multiplica"
    case divide = "op-divide"
}



class ViewController: UIViewController {
    
    
    @IBOutlet var icoSuma: UIImageView!
    @IBOutlet var icoResta: UIImageView!
    @IBOutlet var icoMultiplica: UIImageView!
    @IBOutlet var icoDividir: UIImageView!
    @IBOutlet var lblResult: UILabel!
    
    let currencyFormatter = NumberFormatter()
    
    
    //Guardar resultado de los calculos
    var result : String?{
        didSet{  //inspectores : Cada que se cambie el valor de result se ejecuta el codigo despues de realizar el cambio
            if let result = result{
                
                // se modifique result de refleja en el lable de resultado y con formato de tipo NSNumber
                lblResult.text = currencyFormatter.string(from: NSNumber(value: Double(result.replacingOccurrences(of: ",", with: ".") )!))
            }
        }
        
    }
    
    var previousResult : String?
    var previousOperation : CalcOp?
    
    var currentOperation : CalcOp?{
        willSet{
            resetIcons()
        }
        
        didSet{
            if let currentOperation = currentOperation{
                switch currentOperation {
                case .add:
                    icoSuma.image = UIImage(named: "\(CalcOp.add.rawValue)-on")
                case .substract:
                    icoResta.image = UIImage(named: "\(CalcOp.substract.rawValue)-on")
                case .multiply:
                    icoMultiplica.image = UIImage(named: "\(CalcOp.multiply.rawValue)-on")
                case .divide:
                    icoDividir.image = UIImage(named: "\(CalcOp.divide.rawValue)-on")
                }
            }else{
                resetIcons()
            }
        }
    
    
    }
    
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        
        if let key = sender.titleLabel?.text{
            
            switch key {
            case "0","1","2","3","4","5","6","7","8","9":
                if var newResult = result{
                    newResult.append(key)
                    result = newResult
                }else{
                    // Si todavia no hay nada tecleado en la calculadora
                    result = key
                }
                
            case ",":
                if var newResult = result{
                    if newResult.range(of: ",") == nil{
                        newResult.append(".")
                        result = newResult
                    }
                }
            case "+":
                currentOperation = .add
                applyOperation()
            case "-":
                currentOperation = .substract
                applyOperation()

            case "x":
                currentOperation = .multiply
                applyOperation()

            case "÷":
                currentOperation = .divide
                applyOperation()

            case "=":
                applyOperation()
                currentOperation = nil
                previousOperation = nil
                // previousResult = nil
            
            case "AC":
                resetCalculator()
            
            case "%":
                if var newResult = result, let currentValue = Double(newResult){
                    newResult = String(currentValue / 100)
                    result = newResult
                }
                
            case "±":
                if var newResult = result, let currentValue = Double(newResult) {
                    newResult = String(-currentValue)
                    result = newResult
                }
                
            default:
                break
                
            }
            
        }
        
    }
    
    func applyOperation(){
        
        if result != nil{
            if var newResult = result, let prevOp = previousOperation, let prevResult = previousResult, let dblPreviosResult = Double(prevResult),let dblResult = Double(newResult){
                switch prevOp {
                case .add:
                    newResult = String(dblPreviosResult + dblResult)
                case .substract:
                    newResult = String(dblPreviosResult - dblResult)
                case .multiply:
                    newResult = String(dblPreviosResult * dblResult)
                case .divide:
                    if dblResult != 0{
                        newResult = String(dblPreviosResult / dblResult)
                    }else{
                        newResult = "Error Zero"
                    }
                }
                
                result = newResult
        }
            previousResult = result
            result = nil
        }
        previousOperation = currentOperation

    }
    
    func resetIcons(){
        icoSuma.image = UIImage(named: CalcOp.add.rawValue)
        icoResta.image = UIImage(named: CalcOp.substract.rawValue)
        icoMultiplica.image = UIImage(named: CalcOp.multiply.rawValue)
        icoDividir.image = UIImage(named: CalcOp.divide.rawValue)
    }
    
    func resetCalculator(){
        resetIcons()
        result = nil
        previousResult = nil
        currentOperation = nil
        previousOperation = nil
        lblResult.text = "0"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //configuramos el numberformatter()
        resetCalculator()
        currencyFormatter.usesGroupingSeparator = true  //Colocar separador de miles
        currencyFormatter.maximumFractionDigits  = 6  // no admita mas de 6 decimales
        currencyFormatter.numberStyle = .decimal
        currencyFormatter.locale = Locale.current

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override var prefersStatusBarHidden: Bool{
        return true
    }


}

